package com.das.biometrickeyboardtest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private int count = 0;
    TextView pwd, userid, avgText;

    private Keyboard mKeyboard;
    public static final String MyPREFERENCES = "MyPrefs";
    public static final String Name = "nameKey";
    public static final String Pass = "passKey";
    public static final String First = "firstKey";
    public static final String AVGF = "avgKeyF";
    public static final String AVGs = "avgKeyS";
    SharedPreferences sharedpreferences;
    private TextView submit;
    private String mUserId, mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userid = findViewById(R.id.userId);
        pwd = findViewById(R.id.password);
        avgText = findViewById(R.id.avg_item);
        //       mKeyboard = new Keyboard(this, R.xml.keyboard);

//        pwd.setOnClickListener(v -> {
//            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//
//        });

//        mKeyboardView = (CustomKeyboardView) findViewById(R.id.keyboard_view);
//        mKeyboardView.setKeyboard(mKeyboard);
//        mKeyboardView
//                .setOnKeyboardActionListener(new BasicOnKeyboardActionListener(
//                        this));
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userid.getText() != null)
                    mUserId = userid.getText().toString();
                if (pwd.getText() != null)
                    mPassword = pwd.getText().toString();

                if (TextUtils.isEmpty(mUserId)) {
                    userid.setError("This field is required.");
                    userid.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(mPassword)) {
                    pwd.setError("This field is required.");
                    pwd.requestFocus();
                    return;
                }
                if (mUserId.equals("Rajni")) {
                    if (mPassword.equals("12345")) {

                        String first = sharedpreferences.getString(MainActivity.First, "");

                        if (first.equals("")) {

                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString(Name, mUserId);
                            editor.putString(Pass, mPassword);
                            editor.putString(First, "2");
                            editor.commit();
                            userid.setText("");
                            pwd.setText("");

                            Toast.makeText(MainActivity.this, "Login Successful.", Toast.LENGTH_LONG).show();
                        } else {
                            float avgf = Float.parseFloat(sharedpreferences.getString(AVGF, ""));
                            float avgs = Float.parseFloat(sharedpreferences.getString(AVGs, ""));
                            Log.e("avgf avl==", avgf + "");
                            Log.e("avgs avl==", avgs + "");
                            if (avgs <= (avgf + .05) && avgs >= (avgs - .05)) {
                                Toast.makeText(MainActivity.this, "Login Successful.", Toast.LENGTH_LONG).show();
                                userid.setText("");
                                pwd.setText("");
                                avgText.setText("Submitted pressure: " + avgf + "  and your pressure is: " + avgs);
                            } else {
                                Toast.makeText(MainActivity.this, "You are not the right person.", Toast.LENGTH_LONG).show();
                            }
                        }

                    } else {
                        Toast.makeText(MainActivity.this, "Please enter correct Password.", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Please enter correct User Id.", Toast.LENGTH_LONG).show();
                }

            }
        });


        /**
         * Mostra la tastiera a schermo con una animazione di slide dal basso
         */
//    private void showKeyboardWithAnimation() {
//
//        if (mKeyboardView.getVisibility() == View.GONE) {
//
//            Animation animation = AnimationUtils
//                    .loadAnimation(MainActivity.this,
//                            R.anim.slide_in_bottom);
//            mKeyboardView.showWithAnimation(animation);
//        }
//    }

    }
}