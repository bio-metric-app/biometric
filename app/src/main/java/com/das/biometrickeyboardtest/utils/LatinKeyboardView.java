package com.das.biometrickeyboardtest.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodSubtype;

import com.das.biometrickeyboardtest.MainActivity;
import com.das.biometrickeyboardtest.database.BiometricDB;

/**
 * Created by Rajni on 7/7/22.
 */
public class LatinKeyboardView extends KeyboardView {
    static final int KEYCODE_OPTIONS = -100;
    static final int KEYCODE_LANGUAGE_SWITCH = -101;
    static final int KEYCODE_API = -600;

    public LatinKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LatinKeyboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected boolean onLongPress(Key key) {
        if (key.codes[0] == Keyboard.KEYCODE_CANCEL) {
            getOnKeyboardActionListener().onKey(KEYCODE_OPTIONS, null);
            return true;
        } else {
            return super.onLongPress(key);
        }
    }

    void setSubtypeOnSpaceKey(final InputMethodSubtype subtype) {
        final LatinKeyboard keyboard = (LatinKeyboard) getKeyboard();
        // keyboard.setSpaceIcon(getResources().getDrawable(subtype.getIconResId()));
        // keyboard.setShifted(true);
        invalidateAllKeys();
    }

    int first=0;
    public float prPress,newPress,avgpress;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        long down=0;
        long up=0;
        long down_up=0;
        switch(event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                down=System.currentTimeMillis();
                Log.e("Action::","Down");
                break;
            case MotionEvent.ACTION_UP:
                up=System.currentTimeMillis();
                Log.e("Action::","Down");
                break;
        }
        down_up=up-down;
        Log.e("Left","Screen");
        Log.e("Down time=",down+"");
        Log.e("Up Time==",up+"");
        Log.e("Down up time==",down_up+"");
        Log.e("Pressure==",event.getPressure()+"");
        Log.e("event x==",event.getX()+"");
        Log.e("event y==",event.getY()+"");
        float finX=event.getX();
        float finY=event.getY();

        newPress= event.getPressure();
        if(first==0)
        {
            prPress=newPress;
            avgpress=(newPress+prPress)/2;
        }
        else
        {
            prPress=avgpress;
            avgpress=(newPress+prPress)/2;
        }
        Log.e("newPress val==",newPress+"");
        Log.e("prPress val==",prPress+"");
        Log.e("avgPress val==",avgpress+"");
        SharedPreferences sh= getContext().getSharedPreferences(MainActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        String first=sh.getString(MainActivity.First,"");
        if(first.equals("")) {
            Log.e("First", first + "");
            SharedPreferences.Editor editor = sh.edit();
            editor.putString(MainActivity.AVGF, avgpress+"");
            editor.commit();
        }
        else {
            Log.e("First else", first);
            SharedPreferences.Editor editor = sh.edit();
            editor.putString(MainActivity.AVGs, avgpress+"");
            editor.commit();
        }

        BiometricDB db=new BiometricDB(getContext().getApplicationContext());
        db.open();
        ContentValues cv=new ContentValues();
        cv.put("fDown", down);
        cv.put("fUp", up);
        cv.put("fDownUp", down_up);
        cv.put("fingerSizeX", finX);
        cv.put("fingerSizeY", finY);
        cv.put("pressure", avgpress);
        cv.put("status","1");
        db.insert("add_data", cv);

        return super.onTouchEvent(event);
    }


}
